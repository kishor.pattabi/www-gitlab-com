---
layout: secure_and_protect_direction
title: Product Stage Direction - Protect
description: "GitLab Protect includes features to identify and protect your cloud infrastructure from security vulnerabilities. Learn more!"
canonical_path: "/direction/protect/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Protecting cloud-native applications, services, and infrastructure</b>
    </font>
</p>

<%= partial("direction/protect/templates/overview") %>

<%= devops_diagram(["Protect"]) %>

## Stage Overview

The Protect Stage focuses on providing security visibility across the entire DevSecOps lifecycle as well as monitoring
vulnerabilities in your cloud-native environment.  Protect reduces overall security risk
by enabling you to be ready now for the cloud-native application stack and DevSecOps best practices of the future.
This is accomplished by:

* regularly scanning the containers deployed within your cloud-native environment for vulnerabilities
* consistently enforcing standard security policies across your organization to maximize visibility and coverage of risks

### Groups

The Protect Stage is made up of one group supporting the major categories focused on cloud-native security:

* Container Security - Monitor your cloud-native containers for vulnerabilities to improve your overall security posture.

### Resourcing and Investment

The existing team members for the Protect Stage can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=protect-section)
* [User Experience](https://about.gitlab.com/company/team/?department=protect-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=protect-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=sec-modelops-qe-team)

## 3 Year Stage Themes

<%= partial("direction/protect/templates/themes") %>

## 3 Year Strategy

Building on those themes, some specific capabilities that we envision developing over the next 3 years include the following:

** Security Orchestration **

1. A unified policy management experience for all security policies, spanning across GitLab&apos;s stages. The policy editor will be both versatile and easy to use.  By providing clear information about what policies are deployed, misconfigurations and accidental security holes can be avoided.
1. The ability to detect and notify users about newly discovered vulnerabilities in production or in the default branch.
1. The ability to mitigate threats in production based on vulnerabilities detected during development.

** Scanning **

1. Turnkey scanning of container images that runs by default whenever possible.  This includes continuously scanning the container registry whenever either an image is updated or when the vulnerability database is updated.
1. Extending scanning capabilities into production.  Ideally GitLab will provide a robust workflow for executing a wide variety of scans in production environments, including Container Scanning, Dependency Scanning, Infrastructure Scanning, DAST, and API Scanning.
1. Use of a variety of techniques including grouping, prioritization, and false positive detection to minimize the noise of scans and to allow teams to focus on fixable vulnerabilities that legitimately represent a threat to their environment.

## 1 Year Plan

Over the next 12 months, the Protect stage is focused on expanding the capabilities of the Security Orchestration and Container Scanning categories.  Some of the key initiatives include the following:

* Providing security policy management capabilities at the group and workspace levels
* Extending security policy management capabilities to cover all of GitLab's security scanners
* Improved container scanning functionality, including GA support for the ability to scan container images that are running in a production Kubernetes instance
* Automatic scanning of container images stored in GitLab's container registry

### What We're Not Doing

Although we will likely address many of these areas in the future (as described above in our [3 year strategy](/direction/protect/#3-year-strategy)), we are not planning to make progress on the following initiatives in the next 12 months:
* Adding support for scanning applications running in serverless environments
* Adding support for scanning applications running in bare-metal / non-containerized environments
* Attempting to build our own Security Information and Event Management (SIEM) system
* Building analytics or algorithms to auto-tune or auto-recommend policy improvements

## Competitive Landscape

TBD - this area is currently being re-evaluated

## Key Performance Metrics

The following metrics are used to evaluate the success of the Protect stage:

* **Stage Monthly Active Users**: This is the total number of users that have run at least one Container Scanning job in the last 28 days of the given month.

Note: We are not currently evaluating the success of the Protect stage based on the Security Orchestration category as we do not yet have reliable metrics for this area.  The Stage Monthly Active Cluster metric has been temporarily deprecated due to its impact on performance.

## Target Audience
GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the opportunities listed above, the Protect Stage has features that make it useful to the following personas today.
1. 🟨 [Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
1. ⬜️ [DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) / DevOps Teams
1. ⬜️ Security Consultants

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables SecOps to work collaboratively with DevOps and development to mitigate vulnerabilities in production environments.
1. 🟩 [Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
1. 🟨 [DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) / DevOps Teams
1. 🟨 Security Consultants

### Security Teams
Security teams are the primarily day-to-day users of Protect features.  Over time, GitLab aims to become the primary tool they use to protect, monitor, and manage the security of their production environments.  As most organizations have limited resources and/or security talent, we will strive to make the entire user experience as simple and easy to use as possible so as to minimize the skill set and number of individuals required.  Additionally, summary-level dashboards and reports will eventually be provided to allow for clear reporting up to executives.

Personas

* [Sam - Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
* [Cameron (Compliance Manager)](/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
* [Delaney (Development Team Lead)](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [Sasha (Software Developer)](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Devon (DevOps Engineer)](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* Internal Security Analyst (primarily responsible for insider threat monitoring)

### User Adoption Journey

Protect&apos;s Container Scanning and Security Orchestration categories are intended to be used by a wide range of personas, including Software Developers, Compliance Managers, DevOps Engineers, and Security Engineers.  To adopt these categories, the only prerequisite is for the user to adopt GitLab&apos;s CI pipelines.  For Container Scanning, users must also either use GitLab&apos;s Registry, or use another external registry, or have a Kubernetes instance connected to GitLab.

## Pricing

<%= partial("direction/protect/templates/pricing") %>

<%= partial("direction/categories", :locals => { :stageKey => "protect" }) %>

## Other Top Level Features

The Security Orchestration category&apos;s features span across multiple stages in GitLab, including the Secure and Protect stages:

#### Alert Dashboard Vision (not currently available today)
The alert dashboard is the central location for viewing and managing alerts.  The long-term plan is to aggregate alerts from all categories at the project, group, and workspace levels and visualizing those alerts in both a list-view as well through explorable charts and graphs.  Alerts will be eventually be capable of being prioritized based on the severity of the alert (defined by the policy that generated the alert) as well as an analytical algorithm that scores the alert based on its level of risk.  The alert dashboard will also host the central workflow for taking action on alerts, including any auto-suggested remediation actions or recommended policy tuning changes.

#### Policy Management Vision
The policy management experience is the central location for policies across both the Secure and Protect stages.  Here users will be able to have the flexibility of managing policies directly in code or in a streamlined UI editor.  As part of the long-term vision for the policy management experience, users will be able to view a complete history of all changes and easily revert to a previous version.  A two-step approval process can optionally be enforced for all policy changes.  Eventually the policy management UI will be extended to provide visibility into the performance overhead of each policy.  Suggestions into policy adjustments that might help either reduce false positives or increase overall security coverage will be provided in this section as well.

#### Security Approvals Vision
Security approvals define when and how security teams get involved in the development workflow.  The vision for this area is to provide a highly granular level of approval definition functionality at the project, group, and workspace levels.  These capabilities will be tightly integrated with the Security Policy editor to allow for separation of duties for security teams.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "protect" }) %>

<p align="center">
    <i><br />
    Last Reviewed: 2022-07-08<br />
    Last Updated: 2022-07-08
    </i>
</p>
